package xdaili_test

import (
	"crypto/tls"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"bitbucket.org/ai69/xdaili"
)

var (
	proxyOrder  = "YOUR_OWN_ORDER_NUM"
	proxySecret = "YOUR_OWN_PROXY_SECRET"
	apiUrl      = "https://bot.whatismyipaddress.com/"
)

func ExampleNewProxyAuthTransport() {
	upstream := http.DefaultTransport.(*http.Transport)
	upstream.Proxy = xdaili.GetProxyURL()
	upstream.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	transport := xdaili.NewProxyAuthTransport(upstream, proxyOrder, proxySecret)

	client := &http.Client{
		Timeout:   20 * time.Second,
		Transport: transport,
	}

	log.Println("example starts")

	start := time.Now()
	resp, err := client.Get(apiUrl)
	elapsed := time.Since(start)

	if err != nil {
		log.Fatalf("client error: %v", err)
	} else if resp == nil {
		log.Fatalln("got empty response")
	} else if resp.StatusCode < 200 || resp.StatusCode > 299 {
		log.Fatalf("got unexpected response: %v", resp.Status)
	} else {
		defer resp.Body.Close()
	}

	raw, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("got response error: %v", err)
	}

	log.Printf("example ends with valid result, time cost: %v", elapsed)
	log.Printf("content: %s", raw)

	// Output:
}
