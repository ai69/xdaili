/*
Package xdaili provides a custom http.Transport and helpers for dynamic proxy service powered by xdaili.cn .
*/
package xdaili

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	yh "github.com/1set/gut/yhash"
)

const (
	proxyUrlRaw   = "http://forward.xdaili.cn:80"
	authHeaderKey = "Proxy-Authorization"
)

type ProxyAuthTransport struct {
	http.RoundTripper
	order  string
	secret string
}

// NewProxyAuthTransport returns a modified http.Transport with authentication required by http://www.xdaili.cn/ .
func NewProxyAuthTransport(upstream *http.Transport, order, secret string) *ProxyAuthTransport {
	upstream.Proxy = GetProxyURL()
	upstream.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	return &ProxyAuthTransport{upstream, order, secret}
}

// RoundTrip adds authentication signatures in header of requests.
func (ct *ProxyAuthTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	req.Header.Set(GetAuthHeader(ct.order, ct.secret))
	return ct.RoundTripper.RoundTrip(req)
}

// GetAuthHeader returns a HTTP header key-value for authenticated request to use proxy provided by xdaili.
func GetAuthHeader(order, secret string) (string, string) {
	timestamp := time.Now().UTC().Unix()
	signRaw := fmt.Sprintf("orderno=%s,secret=%s,timestamp=%d", order, secret, timestamp)
	sign, err := yh.StringMD5(signRaw)
	if err != nil {
		panic(err)
	}
	authKey := fmt.Sprintf("sign=%s&orderno=%s&timestamp=%d", strings.ToUpper(sign), order, timestamp)
	return authHeaderKey, authKey
}

// GetProxyURL returns a proxy function (for use in a Transport) to use proxy provided by xdaili.
func GetProxyURL() func(*http.Request) (*url.URL, error) {
	if proxyUrl, err := url.Parse(proxyUrlRaw); err != nil {
		panic(err)
	} else {
		return http.ProxyURL(proxyUrl)
	}
}
